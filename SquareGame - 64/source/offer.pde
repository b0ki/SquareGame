class Offer {
  String type; 
  color clr;
  PImage img;
  OfferButton buy;

  Offer(String t, color c, String pic) {
    type = t;
    clr = c;
    img = loadImage(pic);
    buy = new OfferButton();

    if (type == "PLAYER") {
      playerOffers.add(this);
    } else if (type == "BULLET") {
      bulletOffers.add(this);
    } else if (type == "ENEMY") {
      enemyOffers.add(this);
    }
  }
}

void showOffers(String type) {
  int i = 0;
  if (type.equals("PLAYER")) {
    for (Offer pf : playerOffers) {
      fill(255, 220);
      rect(20, 150 * i + 150, 530, 100); 
      image(pf.img, 40, (150 * i + 150) + 20);
      fill(0);
      textSize(16);
      text("R: " + int(red(pf.clr)) + " G: " + int(green(pf.clr)) + " B: " + int(blue(pf.clr)), 150, (150 * i + 150) + 55);
      pf.buy.showOfferButton(420, (150 * i + 150) + 40, 60, 20);
      i += 1;
    }
  } else if (type.equals("ENEMY")) {
    for (Offer ef : enemyOffers) {
      fill(255, 220);
      rect(20, 150 * i + 150, 530, 100);
      image(ef.img, 40, (150 * i + 150) + 20);
      fill(0);
      textSize(16);
      text("R: " + int(red(ef.clr)) + " G: " + int(green(ef.clr)) + " B: " + int(blue(ef.clr)), 150, (150 * i + 150) + 55);
      ef.buy.showOfferButton(420, (150 * i + 150) + 40, 60, 20);
      i += 1;
    }
  } else if (type.equals("BULLET")) {
    for (Offer bf : bulletOffers) {
      fill(255, 220);
      rect(20, 150 * i + 150, 530, 100); 
      image(bf.img, 40, (150 * i + 150) + 20);
      fill(0);
      textSize(16);
      text("R: " + int(red(bf.clr)) + " G: " + int(green(bf.clr)) + " B: " + int(blue(bf.clr)), 150, (150 * i + 150) + 55);
      bf.buy.showOfferButton(420, (150 * i + 150) + 40, 60, 20);
      i += 1;
    }
  }
}
