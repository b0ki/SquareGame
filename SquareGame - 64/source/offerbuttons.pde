class OfferButton {
  int x;
  int y;
  int w;
  int h;
  
  void showOfferButton(int X, int Y, int W, int H) {
    fill(255, 255, 0);
    image(loadImage("/images/equip.png"), X, Y);
    this.x = X;
    this.y = Y;
    this.w = W;
    this.h = H;
  }
  
  boolean hover() {
    if (mouseX >= this.x && this.x + w >= mouseX) {
      if (mouseY >= this.y && this.y + h >= mouseY) {
        return true;
      }    
    }
    return false;
  }
}
