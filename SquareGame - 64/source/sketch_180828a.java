import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class sketch_180828a extends PApplet {

String state = "homescreen";
int coins = 0;

Button exit, shop, start;
Bullet bullet;
Player player;

int score = 0;
ArrayList<Enemy> enemies = new ArrayList<Enemy>();
int reachedbottom = 0;

ArrayList<Offer> playerOffers = new ArrayList<Offer>();
ArrayList<Offer> enemyOffers = new ArrayList<Offer>();
ArrayList<Offer> bulletOffers = new ArrayList<Offer>();
Button pOffers;
Button bOffers;
Button eOffers;

boolean pfshown = false;
boolean efshown = false;
boolean bfshown = false;

Button returnhome, restart;

Button reset = new Button(215, 450, 140, 50);

public void setup() {
  

  start = new Button(35, 345, 500, 50);
  exit = new Button(35, 495, 500, 50);
  shop = new Button(35, 420, 500, 50);

  bullet = new Bullet();
  player = new Player();

  int numEnemies = 4;
  for (int i = 0; i < numEnemies; i++) {
    Enemy e = new Enemy();
    e.addToLst();
  }


  new Offer("PLAYER", color(174, 194, 255, 70), "/images/player1.png");  
  new Offer("PLAYER", color(6, 200, 103, 70), "/images/player2.png");

  new Offer("ENEMY", color(203, 105, 7, 70), "/images/enemy1.png");  
  new Offer("ENEMY", color(186, 24, 154, 70), "/images/enemy2.png");

  new Offer("BULLET", color(201, 176, 5, 70), "/images/bullet1.png");  
  new Offer("BULLET", color(147, 187, 23, 70), "/images/bullet2.png");
}

public void draw() {
  if (state.equals("homescreen")) {
    homescreen();
  } else if (state.equals("ingame")) {
    game();
  } else if (state.equals("gameover")) {
    gameover();
  } else if (state.equals("shop")) {
    openshop();
  }
}

public void homescreen() {
  background(255);

  PImage back = loadImage("/images/back.png");
  image(back, 30, 0);

  start.display("/images/start_normal.png");
  if (start.hover()) {
    start.display("/images/start_hover.png");
  } else {
    start.display("/images/start_normal.png");
  }

  exit.display("/images/exit_normal.png");
  if (exit.hover()) {
    exit.display("/images/exit_hover.png");
  } else {
    exit.display("/images/exit_normal.png");
  }

  shop.display("/images/shop_normal.png");
  if (shop.hover()) {
    shop.display("/images/shop_hover.png");
  } else {
    shop.display("/images/shop_normal.png");
  }

  if (start.hover() && mousePressed == true) {
    state = "ingame";
  }

  if (exit.hover() && mousePressed == true) {
    exit();
  }

  if (shop.hover() && mousePressed == true) {
    state = "shop";
  }
}

public void game () {
  background(255);



  if (keyPressed && (key == 'p' || key == 'P')) { 
    pause();
  } else {
    if (bullet.fired == true) {
      bullet.fire();
    }

    player.show();

    for (Enemy enemy : enemies) {
      enemy.showEnemy();

      if (isCollidingBE(enemy)) {
        score += 1;
        enemy.reset();
      }

      if (isCollidingPE(enemy)) {
        state = "gameover";
      }

      if (enemy.y > height) {
        reachedbottom += 1;
      }
    }

    if (reachedbottom >= 3) {
      state = "gameover";
    }
  }
}

public void gameover() {
  score = 0;
  background(loadImage("/images/gameover_back.png"));

  returnhome = new Button(35, 345, 500, 50);
  restart = new Button(35, 420, 500, 50);

  returnhome.display("/images/gameover_home.png");
  restart.display("/images/gameover_restart.png");

  if (returnhome.hover() == true) {
    returnhome.display("/images/gameover_home_hover.png");
  } else {
    returnhome.display("/images/gameover_home.png");
  }

  if (restart.hover() == true) {
    restart.display("/images/gameover_restart_hover.png");
  } else {
    restart.display("/images/gameover_restart.png");
  }

  bullet.resetBullet();
  player.resetPlayer();
  for (Enemy e : enemies) {
    e.reset();
  }
}

public void openshop() {
  PImage back;
  back = loadImage("/images/shop_back.png");
  background(back);
  reset.display("/images/reset.png");
  Button home = new Button(width / 2 - 25, 20, 50, 50);

  home.display("/images/home.png");
  if (home.hover() && mousePressed) {
    state = "homescreen";
    home = null;
  }

  pOffers = new Button(20, 96, 100, 20);
  eOffers = new Button(121, 96, 100, 20);
  bOffers = new Button(222, 96, 100, 20);

  pOffers.display("/images/player_offers_tab.png");
  eOffers.display("/images/enemy_offers_tab.png");
  bOffers.display("/images/bullet_offers_tab.png");

  stroke(0);
  line(0, 130, 570, 130);

  if (pfshown == true) {
    back = loadImage("/images/fullwhite.png");
    showOffers("PLAYER");
  } else if (efshown == true) {
    back = loadImage("/images/fullwhite.png");
    showOffers("ENEMY");
  } else if (bfshown == true) {
    back = loadImage("/images/fullwhite.png");
    showOffers("BULLET");
  }
}

public void pause() {
  Button home = new Button(width / 2 - 25, 20, 50, 50);
  home.display("/images/home.png");
  if (home.hover() && mousePressed) {
    state = "homescreen";
  }
  stroke(0);
  strokeWeight(1);
  line(100, 85, 470, 85);

  image(loadImage("/images/pause.png"), 0, 100);

  fill(0);
  textSize(36);
  if (score < 10) {
    text(score, width / 2 - 10, 125);
  } else if (score >= 10 && score < 100) {
    text(score, width / 2 - 23, 125);
  } else if (score >= 100 && score <= 1000) {
    text(score, width / 2 - 33, 125);
  }
}

public void mouseClicked() {
  if (state.equals("ingame")) {
    if (bullet.fired == true) {
      bullet.y = player.y - 30 / 2;
      bullet.x = player.x - 10 / 2;
      if (bullet.speed > 3.5f) {
        bullet.speed -= 0.02f;
      }
    } else {
      bullet.fired = true;
      bullet.y = player.y - 30 / 2;
      bullet.x = player.x - 10 / 2;
      if (bullet.speed > 3.5f) {
        bullet.speed -= 0.02f;
      }
    }
  } else if (state.equals("shop")) {

    if (reset.hover() == true) {
      player.resetColor();
      bullet.resetColor();
      for (Enemy e : enemies) {
        e.resetColor();
      }
      println("All colors reset");
    }

    if (pOffers.hover() == true) {
      if (pfshown == true) { 
        pfshown = false;
      } else { 
        efshown = false;
        bfshown = false;
        pfshown = true;
      }
    } else if (eOffers.hover() == true) {
      if (efshown == true) {
        efshown = false;
      } else {
        pfshown = false;
        bfshown = false;
        efshown = true;
      }
    } else if (bOffers.hover() == true) {
      if (bfshown == true) {
        bfshown = false;
      } else {
        efshown = false;
        pfshown = false;
        bfshown = true;
      }
    }

    if (pfshown == true) {
      for (Offer pf : playerOffers) {
        if (pf.buy.hover() == true) {
          player.updateColor(pf.clr);
          println("Bought smth");
        }
      }
    }

    if (efshown == true) {
      for (Offer ef : enemyOffers) {
        if (ef.buy.hover() == true) {
          for (Enemy e : enemies) {
            e.updateColor(ef.clr);
          }
          println("Bought smth");
        }
      }
    }

    if (bfshown == true) {
      for (Offer bf : bulletOffers) {
        if (bf.buy.hover() == true) {
          bullet.updateColor(bf.clr);
          println("Bought smth");
        }
      }
    }
  } else if (state.equals("gameover")) {
    if (returnhome.hover() == true) {
      state = "homescreen";
    } else if (restart.hover() == true) {
      state = "ingame";
    }
  }
}



public boolean isCollidingBE(Enemy enemy) {
  double d = abs(bullet.x - enemy.x) + abs(bullet.y - enemy.y);
  if (d < enemy.size / 2) {
    return true;
  }
  return false;
}

public boolean isCollidingPE(Enemy enemy) {
  double d = abs(player.x - enemy.x) + abs(player.y - enemy.y);
  if (d < enemy.size / 2) {
    return true;
  }
  return false;
}
class Bullet {
  boolean fired = false;
  int x;
  int y = height - 20;
  float speed = 10;
  int startclr = color(0, 255, 0, 100);
  int clr;

  Bullet() {
    this.clr = color(0, 255, 0, 100);
  }
  
  public void resetColor() {
    this.clr = startclr;
  }
  
  public void resetBullet() {
    int x;
    int y = height - 20;
  }

  public void updateColor(int c){
    this.clr = c;
  }

  public void fire() {   
    fill(clr);
    rect(x, y, 10, 10);
    y -= speed;
  }
}
class Button {

  int x;
  int y;
  PImage img;
  int buttonWidth;
  int buttonHeight;
  int c = color(255, 255, 0);

  Button (int X, int Y, int bWidth, int bHeight) {
    x = X;
    y = Y;
    buttonWidth = bWidth;
    buttonHeight = bHeight;
    
  } 

  public void display(String image) {
    img = loadImage(image);
    image(this.img, x, y);
  }

  public void setLabel(String txt, int size, int d) {
    fill(0);
    textSize(size);
    text(txt, x + 5, y + d);
  }

  public boolean hover() {
    if (mouseX >= x && mouseX <= x + buttonWidth) {
      if (mouseY >= y && mouseY <= y + buttonHeight) {
        return true;
      }
    }
    return false;
  }
}
class Enemy {
  int x;
  int y;
  float speed;
  float size;
  float specialnum = 0;
  float oldspeed;
  int clr;
  int startclr = color(255, 0, 0, 100);

  Enemy() {
    speed = random(1, 3);
    size = random(40, 60);
    x = floor(random(size / 2, width - size / 2));  
    y = floor(random(-500, 0));
    clr = color(255, 0, 0, 100);
  }
  
  public void updateColor(int c){
    this.clr = c;
  }
  
  public void resetColor() {
    this.clr = startclr;
  }
  
  public void addToLst() {
    enemies.add(this);
  }

  public void showEnemy() {
    fill(clr);
    rect(x - size / 2, y - size / 2, size, size);
    fill(255);
    ellipse(x, y, 3, 3);
    y += speed;
    
    if (y > height) {
      this.reset();
    }
  }
  
  public void reset() {
    speed = random(1, 3);
    size = random(40, 60);
    x = floor(random(0, width - size));  
    y = floor(random(-500, 0));
    specialnum += 0.03f;
  }
  
  public void pauseMove() {
    oldspeed = speed;
    speed = 0;
  }
  
  public void resumeMove() {
    speed = oldspeed;
    oldspeed = 0;
  }
}
class Offer {
  String type; 
  int clr;
  PImage img;
  OfferButton buy;

  Offer(String t, int c, String pic) {
    type = t;
    clr = c;
    img = loadImage(pic);
    buy = new OfferButton();

    if (type == "PLAYER") {
      playerOffers.add(this);
    } else if (type == "BULLET") {
      bulletOffers.add(this);
    } else if (type == "ENEMY") {
      enemyOffers.add(this);
    }
  }
}

public void showOffers(String type) {
  int i = 0;
  if (type.equals("PLAYER")) {
    for (Offer pf : playerOffers) {
      fill(255, 220);
      rect(20, 150 * i + 150, 530, 100); 
      image(pf.img, 40, (150 * i + 150) + 20);
      fill(0);
      textSize(16);
      text("R: " + PApplet.parseInt(red(pf.clr)) + " G: " + PApplet.parseInt(green(pf.clr)) + " B: " + PApplet.parseInt(blue(pf.clr)), 150, (150 * i + 150) + 55);
      pf.buy.showOfferButton(420, (150 * i + 150) + 40, 60, 20);
      i += 1;
    }
  } else if (type.equals("ENEMY")) {
    for (Offer ef : enemyOffers) {
      fill(255, 220);
      rect(20, 150 * i + 150, 530, 100);
      image(ef.img, 40, (150 * i + 150) + 20);
      fill(0);
      textSize(16);
      text("R: " + PApplet.parseInt(red(ef.clr)) + " G: " + PApplet.parseInt(green(ef.clr)) + " B: " + PApplet.parseInt(blue(ef.clr)), 150, (150 * i + 150) + 55);
      ef.buy.showOfferButton(420, (150 * i + 150) + 40, 60, 20);
      i += 1;
    }
  } else if (type.equals("BULLET")) {
    for (Offer bf : bulletOffers) {
      fill(255, 220);
      rect(20, 150 * i + 150, 530, 100); 
      image(bf.img, 40, (150 * i + 150) + 20);
      fill(0);
      textSize(16);
      text("R: " + PApplet.parseInt(red(bf.clr)) + " G: " + PApplet.parseInt(green(bf.clr)) + " B: " + PApplet.parseInt(blue(bf.clr)), 150, (150 * i + 150) + 55);
      bf.buy.showOfferButton(420, (150 * i + 150) + 40, 60, 20);
      i += 1;
    }
  }
}
class OfferButton {
  int x;
  int y;
  int w;
  int h;
  
  public void showOfferButton(int X, int Y, int W, int H) {
    fill(255, 255, 0);
    image(loadImage("/images/equip.png"), X, Y);
    this.x = X;
    this.y = Y;
    this.w = W;
    this.h = H;
  }
  
  public boolean hover() {
    if (mouseX >= this.x && this.x + w >= mouseX) {
      if (mouseY >= this.y && this.y + h >= mouseY) {
        return true;
      }    
    }
    return false;
  }
}
class Player {
  int x;
  int y;
  String img;
  int clr;
  int startclr = color(0, 0, 255, 100);


  Player() {
    x = mouseX;
    y = height - 40;
    clr = color(0, 0, 255, 100);
  }

  public void updateColor(int c) {
    this.clr = c;
  }

  public void resetPlayer() {
    x = mouseX;
    y = height - 40;
  }

  public void resetColor() {
    this.clr = startclr;
  }

  public void show() {
    x = mouseX;
    fill(clr);
    rect(x - 30 / 2, y - 30 / 2, 30, 30);
  }

  public void pausePlayer() {
    mouseX = x;
    fill(0, 0, 255, 100);
    rect(x - 30 / 2, y - 30 / 2, 30, 30);
  }
}
  public void settings() {  size(570, 570); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--present", "--window-color=#FFFFFF", "--stop-color=#000000", "sketch_180828a" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
