class Enemy {
  int x;
  int y;
  float speed;
  float size;
  float specialnum = 0;
  float oldspeed;
  color clr;
  color startclr = color(255, 0, 0, 100);

  Enemy() {
    speed = random(1, 3);
    size = random(40, 60);
    x = floor(random(size / 2, width - size / 2));  
    y = floor(random(-500, 0));
    clr = color(255, 0, 0, 100);
  }
  
  void updateColor(color c){
    this.clr = c;
  }
  
  void resetColor() {
    this.clr = startclr;
  }
  
  void addToLst() {
    enemies.add(this);
  }

  void showEnemy() {
    fill(clr);
    rect(x - size / 2, y - size / 2, size, size);
    fill(255);
    ellipse(x, y, 3, 3);
    y += speed;
    
    if (y > height) {
      this.reset();
    }
  }
  
  void reset() {
    speed = random(1, 3);
    size = random(40, 60);
    x = floor(random(0, width - size));  
    y = floor(random(-500, 0));
    specialnum += 0.03;
  }
  
  void pauseMove() {
    oldspeed = speed;
    speed = 0;
  }
  
  void resumeMove() {
    speed = oldspeed;
    oldspeed = 0;
  }
}
