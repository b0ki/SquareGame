class Bullet {
  boolean fired = false;
  int x;
  int y = height - 20;
  float speed = 10;
  color startclr = color(0, 255, 0, 100);
  color clr;

  Bullet() {
    this.clr = color(0, 255, 0, 100);
  }
  
  void resetColor() {
    this.clr = startclr;
  }
  
  void resetBullet() {
    int x;
    int y = height - 20;
  }

  void updateColor(color c){
    this.clr = c;
  }

  void fire() {   
    fill(clr);
    rect(x, y, 10, 10);
    y -= speed;
  }
}
