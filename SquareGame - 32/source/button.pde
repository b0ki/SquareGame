class Button {

  int x;
  int y;
  PImage img;
  int buttonWidth;
  int buttonHeight;
  color c = color(255, 255, 0);

  Button (int X, int Y, int bWidth, int bHeight) {
    x = X;
    y = Y;
    buttonWidth = bWidth;
    buttonHeight = bHeight;
    
  } 

  void display(String image) {
    img = loadImage(image);
    image(this.img, x, y);
  }

  void setLabel(String txt, int size, int d) {
    fill(0);
    textSize(size);
    text(txt, x + 5, y + d);
  }

  boolean hover() {
    if (mouseX >= x && mouseX <= x + buttonWidth) {
      if (mouseY >= y && mouseY <= y + buttonHeight) {
        return true;
      }
    }
    return false;
  }
}
