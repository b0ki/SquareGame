class Player {
  int x;
  int y;
  String img;
  color clr;
  color startclr = color(0, 0, 255, 100);


  Player() {
    x = mouseX;
    y = height - 40;
    clr = color(0, 0, 255, 100);
  }

  void updateColor(color c) {
    this.clr = c;
  }

  void resetPlayer() {
    x = mouseX;
    y = height - 40;
  }

  void resetColor() {
    this.clr = startclr;
  }

  void show() {
    x = mouseX;
    fill(clr);
    rect(x - 30 / 2, y - 30 / 2, 30, 30);
  }

  void pausePlayer() {
    mouseX = x;
    fill(0, 0, 255, 100);
    rect(x - 30 / 2, y - 30 / 2, 30, 30);
  }
}
