String state = "homescreen";
int coins = 0;

Button exit, shop, start;
Bullet bullet;
Player player;

int score = 0;
ArrayList<Enemy> enemies = new ArrayList<Enemy>();
int reachedbottom = 0;

ArrayList<Offer> playerOffers = new ArrayList<Offer>();
ArrayList<Offer> enemyOffers = new ArrayList<Offer>();
ArrayList<Offer> bulletOffers = new ArrayList<Offer>();
Button pOffers;
Button bOffers;
Button eOffers;

boolean pfshown = false;
boolean efshown = false;
boolean bfshown = false;

Button returnhome, restart;

Button reset = new Button(215, 450, 140, 50);

void setup() {
  size(570, 570);

  start = new Button(35, 345, 500, 50);
  exit = new Button(35, 495, 500, 50);
  shop = new Button(35, 420, 500, 50);

  bullet = new Bullet();
  player = new Player();

  int numEnemies = 4;
  for (int i = 0; i < numEnemies; i++) {
    Enemy e = new Enemy();
    e.addToLst();
  }


  new Offer("PLAYER", color(174, 194, 255, 70), "/images/player1.png");  
  new Offer("PLAYER", color(6, 200, 103, 70), "/images/player2.png");

  new Offer("ENEMY", color(203, 105, 7, 70), "/images/enemy1.png");  
  new Offer("ENEMY", color(186, 24, 154, 70), "/images/enemy2.png");

  new Offer("BULLET", color(201, 176, 5, 70), "/images/bullet1.png");  
  new Offer("BULLET", color(147, 187, 23, 70), "/images/bullet2.png");
}

void draw() {
  if (state.equals("homescreen")) {
    homescreen();
  } else if (state.equals("ingame")) {
    game();
  } else if (state.equals("gameover")) {
    gameover();
  } else if (state.equals("shop")) {
    openshop();
  }
}

void homescreen() {
  background(255);

  PImage back = loadImage("/images/back.png");
  image(back, 30, 0);

  start.display("/images/start_normal.png");
  if (start.hover()) {
    start.display("/images/start_hover.png");
  } else {
    start.display("/images/start_normal.png");
  }

  exit.display("/images/exit_normal.png");
  if (exit.hover()) {
    exit.display("/images/exit_hover.png");
  } else {
    exit.display("/images/exit_normal.png");
  }

  shop.display("/images/shop_normal.png");
  if (shop.hover()) {
    shop.display("/images/shop_hover.png");
  } else {
    shop.display("/images/shop_normal.png");
  }

  if (start.hover() && mousePressed == true) {
    state = "ingame";
  }

  if (exit.hover() && mousePressed == true) {
    exit();
  }

  if (shop.hover() && mousePressed == true) {
    state = "shop";
  }
}

void game () {
  background(255);



  if (keyPressed && (key == 'p' || key == 'P')) { 
    pause();
  } else {
    if (bullet.fired == true) {
      bullet.fire();
    }

    player.show();

    for (Enemy enemy : enemies) {
      enemy.showEnemy();

      if (isCollidingBE(enemy)) {
        score += 1;
        enemy.reset();
      }

      if (isCollidingPE(enemy)) {
        state = "gameover";
      }

      if (enemy.y > height) {
        reachedbottom += 1;
      }
    }

    if (reachedbottom >= 3) {
      state = "gameover";
    }
  }
}

void gameover() {
  score = 0;
  background(loadImage("/images/gameover_back.png"));

  returnhome = new Button(35, 345, 500, 50);
  restart = new Button(35, 420, 500, 50);

  returnhome.display("/images/gameover_home.png");
  restart.display("/images/gameover_restart.png");

  if (returnhome.hover() == true) {
    returnhome.display("/images/gameover_home_hover.png");
  } else {
    returnhome.display("/images/gameover_home.png");
  }

  if (restart.hover() == true) {
    restart.display("/images/gameover_restart_hover.png");
  } else {
    restart.display("/images/gameover_restart.png");
  }

  bullet.resetBullet();
  player.resetPlayer();
  for (Enemy e : enemies) {
    e.reset();
  }
}

void openshop() {
  PImage back;
  back = loadImage("/images/shop_back.png");
  background(back);
  reset.display("/images/reset.png");
  Button home = new Button(width / 2 - 25, 20, 50, 50);

  home.display("/images/home.png");
  if (home.hover() && mousePressed) {
    state = "homescreen";
    home = null;
  }

  pOffers = new Button(20, 96, 100, 20);
  eOffers = new Button(121, 96, 100, 20);
  bOffers = new Button(222, 96, 100, 20);

  pOffers.display("/images/player_offers_tab.png");
  eOffers.display("/images/enemy_offers_tab.png");
  bOffers.display("/images/bullet_offers_tab.png");

  stroke(0);
  line(0, 130, 570, 130);

  if (pfshown == true) {
    back = loadImage("/images/fullwhite.png");
    showOffers("PLAYER");
  } else if (efshown == true) {
    back = loadImage("/images/fullwhite.png");
    showOffers("ENEMY");
  } else if (bfshown == true) {
    back = loadImage("/images/fullwhite.png");
    showOffers("BULLET");
  }
}

void pause() {
  Button home = new Button(width / 2 - 25, 20, 50, 50);
  home.display("/images/home.png");
  if (home.hover() && mousePressed) {
    state = "homescreen";
  }
  stroke(0);
  strokeWeight(1);
  line(100, 85, 470, 85);

  image(loadImage("/images/pause.png"), 0, 100);

  fill(0);
  textSize(36);
  if (score < 10) {
    text(score, width / 2 - 10, 125);
  } else if (score >= 10 && score < 100) {
    text(score, width / 2 - 23, 125);
  } else if (score >= 100 && score <= 1000) {
    text(score, width / 2 - 33, 125);
  }
}

void mouseClicked() {
  if (state.equals("ingame")) {
    if (bullet.fired == true) {
      bullet.y = player.y - 30 / 2;
      bullet.x = player.x - 10 / 2;
      if (bullet.speed > 3.5) {
        bullet.speed -= 0.02;
      }
    } else {
      bullet.fired = true;
      bullet.y = player.y - 30 / 2;
      bullet.x = player.x - 10 / 2;
      if (bullet.speed > 3.5) {
        bullet.speed -= 0.02;
      }
    }
  } else if (state.equals("shop")) {

    if (reset.hover() == true) {
      player.resetColor();
      bullet.resetColor();
      for (Enemy e : enemies) {
        e.resetColor();
      }
      println("All colors reset");
    }

    if (pOffers.hover() == true) {
      if (pfshown == true) { 
        pfshown = false;
      } else { 
        efshown = false;
        bfshown = false;
        pfshown = true;
      }
    } else if (eOffers.hover() == true) {
      if (efshown == true) {
        efshown = false;
      } else {
        pfshown = false;
        bfshown = false;
        efshown = true;
      }
    } else if (bOffers.hover() == true) {
      if (bfshown == true) {
        bfshown = false;
      } else {
        efshown = false;
        pfshown = false;
        bfshown = true;
      }
    }

    if (pfshown == true) {
      for (Offer pf : playerOffers) {
        if (pf.buy.hover() == true) {
          player.updateColor(pf.clr);
          println("Bought smth");
        }
      }
    }

    if (efshown == true) {
      for (Offer ef : enemyOffers) {
        if (ef.buy.hover() == true) {
          for (Enemy e : enemies) {
            e.updateColor(ef.clr);
          }
          println("Bought smth");
        }
      }
    }

    if (bfshown == true) {
      for (Offer bf : bulletOffers) {
        if (bf.buy.hover() == true) {
          bullet.updateColor(bf.clr);
          println("Bought smth");
        }
      }
    }
  } else if (state.equals("gameover")) {
    if (returnhome.hover() == true) {
      state = "homescreen";
    } else if (restart.hover() == true) {
      state = "ingame";
    }
  }
}



boolean isCollidingBE(Enemy enemy) {
  double d = abs(bullet.x - enemy.x) + abs(bullet.y - enemy.y);
  if (d < enemy.size / 2) {
    return true;
  }
  return false;
}

boolean isCollidingPE(Enemy enemy) {
  double d = abs(player.x - enemy.x) + abs(player.y - enemy.y);
  if (d < enemy.size / 2) {
    return true;
  }
  return false;
}
